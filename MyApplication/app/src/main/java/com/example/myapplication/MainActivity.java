package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // Comprueba si la app está en modo overview screen
        if (!hasFocus) {
            // Muestra la vista que tenía la app antes
            setContentView(R.layout.overlay_layout);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Muestra la vista que tenía la app antes
        setContentView(R.layout.activity_main);
    }
}
