import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
public class OverlayView extends RelativeLayout{
    private ImageView imageView;
    public OverlayView(Context context) {
        super(context);

        // Inflar el diseño de la superposición
        LayoutInflater inflater = LayoutInflater.from(context);
        View overlayView = inflater.inflate(R.layout.overlay_layout, this, true);

        // Configurar la imagen de la superposición
        imageView = overlayView.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.mila_azul);  // Reemplaza "your_image" con tu imagen
    }

    public void show() {
        setVisibility(VISIBLE);
    }

    public void hide() {
        setVisibility(GONE);
    }
}
